<?php
session_start();
if(!isset($_SESSION['login'])) {
    echo '<h1>Vous n\'êtes pas connecté, accés interdit !</h1>';
    header("refresh:3;URL= ./index.php");
  }
  else{
    include('./classes/musique.php');
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/styleplaylist.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Playlist</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="./forms/disconnect.php">Déconnexion</a>
      </li>
    </ul>
  </div>
</nav>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="<?php
  $linked = substr($_POST['link'], 32,11);
 echo "https://www.youtube.com/embed/$linked?rel=0&autoplay=1";?>" frameborder="0" allow="autoplay; encrypted-media"></iframe>
</div>
<div class="list-group">
<?php
$musique = Musique::all();
foreach($musique as $value){
?>
  <form action="playlist.php" method="POST">
  <button type="submit" class="list-group-item list-group-item-action"><?php echo $value['title']; ?></button>
  <input type="hidden" name="link" value="<?php echo $value['link'];?>">
  </form>
<?php } ?>
</div>
  <div class="addMusique">
<form action="./forms/addMusique.php" method="GET">
  <div class="form-group">
    <input type="name" class="form-control" id="exampleInputEmail1" name="musique" aria-describedby="emailHelp" placeholder="Lien/URL Youtube">
    <input type="hidden" name="iduser" value="<?php echo $_SESSION['id']?>">
    </div>
    <button type="submit" class="btn btn-primary">Ajouter</button>
</form>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>
<?php
  }
?>