<?php

include(__DIR__.'/../classes/DB.php');

class Utilisateur
{

    public $login;
    public $password;
    public $email;
    public $pseudo;

    function __construct($login,$password,$email,$pseudo){
        $this->setLogin($login);
        $this->setPassword($password);
        $this->setEmail($email);
        $this->setPseudo($pseudo);
    }

    //-------setters-------//

    public function setLogin($value){
        $this->login = $value;
    }

    public function setPassword($value){
        $this->password = $value;
    }

    public function setEmail($value){
        $this->email = $value;
    }

    public function setPseudo($value){
        $this->pseudo = $value;
    }

    //------getters-------//

    public function getLogin() {
        return $this->login;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPseudo() {
        return $this->pseudo;
    }

    //-------function---------//

    public function insert(){
        $db = DB::connection();
        $reqvalidation = $db->prepare("SELECT * FROM utilisateurs WHERE login=:login or email=:email");
        $reqvalidation->execute(array(
            'login'=>$this->getLogin(),
            'email'=>$this->getEmail()
        ));

        $count = $reqvalidation->rowCount();

        if($count == 1){
            echo 'Ce pseudo ou email est déjà utilisé';
            header("refresh:3;URL= ../index.php");
            exit();
        }

        else{
        $insert = $db->prepare('INSERT INTO utilisateurs (login,password,email,pseudo) VALUES (:login,:password,:email,:pseudo)');
        $result= $insert->execute([
            'login'=>$this->getLogin(),
            'password'=>$this->getPassword(),
            'email'=>$this->getEmail(),
            'pseudo'=>$this->getPseudo()
        ]);

        if (!$result) throw new Exception('La sauvegarde n\'a pas fonctionnée');
        // return
        return $result;
        }
    }

    static function connection($identifiant,$password){
        $db = DB::connection();
        $select =  $db->prepare('SELECT * FROM utilisateurs WHERE login=:login') or die ('Pseudo inconnu!');
        $select->execute([
            'login'=>$identifiant
        ]);
        $fetch = $select->fetch();

        if($fetch['password'] != $password){
            Echo'Mauvais login/password,Merci de recommencer ultérieurement.';
        }

        else{
            session_start();
            $_SESSION['login'] = $identifiant;
            $_SESSION['id'] = $fetch['id'];
            echo 'Vous etes bien connectée.';
            header("refresh:3;URL= ../playlist.php");
        }
    }
}

?>