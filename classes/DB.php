<?php

include(__DIR__.'/../config.php');

class DB {

    public static function connection() {
    // config
    $user = CONFIG['database']['user'];
    $password = CONFIG['database']['password'];
    $host = CONFIG['database']['host' ];
    $database = CONFIG['database']['database'];

    // connection
    $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password); // piftodo
    return $bdd;
    }

}


?>