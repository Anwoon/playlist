<?php

include(__DIR__.'/../classes/DB.php');


class Musique
{
    public $link;
    public $iduser;
    public $title;

    function __construct($link,$iduser,$title){
        $this->setLink($link);
        $this->setIdUser($iduser);
        $this->setTitle($title);
    }

    //-------setters-------//

    public function setLink($value){
        $this->link = $value;
    }

    public function setIdUser($value){
        $this->iduser = $value;
    }

    public function setTitle($value){
        $this->title = $value;
    }

    //------getters-------//

    public function getLink() {
        return $this->link;
    }

    public function getIdUser() {
        return $this->iduser;
    }

    public function getTitle() {
        return $this->title;
    }

    //-------function---------//

    public function insert(){
        $db = DB::connection();
        $insert = $db->prepare('INSERT INTO musiques (link,iduser,title) VALUES (:link,:iduser,:title)');
        $result= $insert->execute([
            'link'=>$this->getLink(),
            'iduser'=>$this->getIdUser(),
            'title'=>$this->getTitle()
        ]);

    }

    public static function all(){
        $db = DB::connection();
        $select = $db->query('SELECT * FROM musiques WHERE iduser='.$_SESSION['id'].'');
        $request = $select->fetchAll();
        return $request;
    }
}

?>