<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Playlist - Inscription</title>
</head>
<body>
<div id="container-connection">
<h3>Inscription</h3>
<form action="./forms/register.php" method="POST">
  <div class="form-group">
   <label for="formGroupExampleInput">Identifiant</label>
    <input type="name" class="form-control" id="exampleInputEmail1" name="identifiant" aria-describedby="emailHelp" placeholder="Enter email">
  </div>
  <div class="form-group">
   <label for="formGroupExampleInput">Mot de passe</label>
    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
   <label for="formGroupExampleInput">Email</label>
    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
   <label for="formGroupExampleInput">Pseudo</label>
    <input type="name" class="form-control" name="pseudo" id="exampleInputEmail1" placeholder="Pseudo">
  </div>
  <button type="submit" class="btn btn-primary">Inscription</button>
  <a href="index.php" class="btn btn-primary" role="button" aria-pressed="true">Annuler</a>
</form>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>